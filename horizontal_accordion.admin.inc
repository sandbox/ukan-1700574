<?php

/**
 * @file
 * Contains back-end functions
 * Make the Horizontal Accodrion fully configurable.
 */


/**
 * Admin Homepage of the module.
 */
function horizontal_accordion_edit_elements_form($form, &$form_state) {
  $form['#tree'] = TRUE;
  $form['#theme'] = 'horizontal_accordion_edit_elements_form';

  $content = horizontal_accordion_get_content();

  if (!empty($content)) {
    foreach ($content as $key => $element) {
      $form['elements'][$key]['title'] = array('#markup' => $element['title']);

      $operations = array();
      $operations['edit'] = l(t('Edit'), 'admin/structure/horizontal_accordion/edit/' .
        $key);
      $operations['delete'] = l(t('Delete'),
        'admin/structure/horizontal_accordion/delete/' . $key);
      $form['elements'][$key]['operations'] = array('#markup' => implode(' - ', $operations));

      $form['elements'][$key]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 10,
        '#attributes' => array('class' => array('weight')),
        '#default_value' => $key);

      $form['#elements'][] = array(
        'weight' => $key,
        'key' => $key,
        'status' => TRUE);
    }
  }

  $form['options_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced options'),
  );

  $form['options_fieldset']['size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Size options'),
  );

  $form['options_fieldset']['size']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Configure the accordion size from either choosing the tab size or the overall accordion size'),
    '#options' => array(
      'tab_size' => t('Select tab size'),
      'accordion_size' => t('Select accordion size'),
    ),
    '#default_value' => horizontal_accordion_get_option('sizingType'),
  );

  $form['options_fieldset']['size']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => horizontal_accordion_get_option('width'),
    '#size' => 4,
    '#maxlength' => '4');

  $form['options_fieldset']['size']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => horizontal_accordion_get_option('height'),
    '#size' => 4,
    '#maxlength' => '4');

  $form['options_fieldset']['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => array(
      'basic' => t('basic'),
      'dark' => t('dark'),
      'light' => t('light'),
      'stitch' => t('stitch'),
      'colorful' => t('colorful'),
    ),
    '#default_value' => horizontal_accordion_get_option('theme'));

  $form['options_fieldset']['first_slide'] = array(
    '#type' => 'select',
    '#title' => t('First Slide'),
    '#options' => array(
      'first' => t('first'),
      'last' => t('last'),
    ),
    '#default_value' => horizontal_accordion_get_option('firstSlide'));

  $form['options_fieldset']['enumerate_slides'] = array(
    '#type' => 'select',
    '#title' => t('Enumerate slides'),
    '#options' => array(
      FALSE => t('false'),
      TRUE => t('true'),
    ),
    '#default_value' => horizontal_accordion_get_option('enumerateSlides'));

  $form['options_fieldset']['auto_play'] = array(
    '#type' => 'select',
    '#title' => t('Auto play'),
    '#options' => array(
      FALSE => t('false'),
      TRUE => t('true'),
    ),
    '#default_value' => horizontal_accordion_get_option('autoPlay'));

  $form['options_fieldset']['easing'] = array(
    '#type' => 'select',
    '#title' => t('Easing effect'),
    '#options' => array(
      'linear' => t('linear'),
      'swing' => t('swing'),
      'easeInQuad' => t('slow at the beginning'),
      'easeOutQuad' => t('slow at the end'),
      'easeInOutQuad' => t('slow at the beginning and the end'),
      'easeOutBounce' => t('bounce at the end')),
    '#default_value' => horizontal_accordion_get_option('easing'));

  $form['options_fieldset']['activate_on'] = array(
    '#type' => 'select',
    '#title' => t('Trigger the slide on'),
    '#options' => array(
      'click' => t('click'),
      'mouseover' => t('mouse over'),
    ),
    '#default_value' => horizontal_accordion_get_option('activateOn'));

  return system_settings_form($form);
}


/**
 * Theming function of horizontal_accordion_edit_elements_form ().
 */
function theme_horizontal_accordion_edit_elements_form(&$vars) {
  $form = &$vars['form'];

  drupal_add_js('misc/tableheader.js');
  drupal_add_tabledrag('element_list', 'order', 'sibling', 'weight', NULL, NULL, TRUE);

  $output = '';

  if (!empty($form['#elements'])) {
    $content = &$form['#elements'];
    $header = array(
      t('Tab title'),
      t('Operations'),
      t('Weight'));

    $rows = array();

    foreach ($content as $element) {
      $row = array();

      $key = $element['key'];

      $row[] = drupal_render($form['elements'][$key]['title']);
      $row[] = drupal_render($form['elements'][$key]['operations']);
      $row[] = drupal_render($form['elements'][$key]['weight']);

      // Merge row.
      $row = array_merge(array('data' => $row), array());
      $row['class'] = array('draggable');
      $rows[] = $row;
    }

    $variables_enabled = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => 'element_list'));

    $output .= theme('table', $variables_enabled);

  }

  $output .= drupal_render_children($form);
  return $output;
}


/**
 * Callback horizontal_accordion_edit_elements_form validate function.
 *
 * Check element weight and advanced parameters.
 */
function horizontal_accordion_edit_elements_form_validate($form, &$form_state) {
  $order = array();

  foreach ($form['#elements'] as $key => $element) {
    $order[$key] = $form_state['values']['elements'][$key]['weight'];
  }

  asort($order);

  $final_order = array();
  $content = horizontal_accordion_get_content();
  foreach ($order as $key => $weight) {
    $final_order[] = $content[$key];
  }

  variable_set('horizontal_accordion_elements', $final_order);

  // Advanced Options.
  $options = variable_get('horizontal_accordion_options', array());
  $options['activateOn'] = $form_state['values']['options_fieldset']['activate_on'];
  $options['autoPlay'] = $form_state['values']['options_fieldset']['auto_play'];
  $options['easing'] = $form_state['values']['options_fieldset']['easing'];
  $options['enumerateSlides'] = $form_state['values']['options_fieldset']['enumerate_slides'];
  $options['firstSlide'] = $form_state['values']['options_fieldset']['first_slide'];
  $options['height'] = $form_state['values']['options_fieldset']['size']['height'];
  $options['sizingType'] = $form_state['values']['options_fieldset']['size']['type'];
  $options['theme'] = $form_state['values']['options_fieldset']['theme'];
  $options['width'] = $form_state['values']['options_fieldset']['size']['width'];

  horizontal_accordion_save_options($options);

  horizontal_accordion_image_style_update();
}

/**
 * Function to edit a specific element or create a new one.
 */
function horizontal_accordion_edit_specific_element($form, &$form_state) {
  $id = arg(4);

  $content = horizontal_accordion_get_content();
  if (isset($content[$id])) {
    $values = $content[$id];
    $image_display = ($values['type'] == 'image') ? '' : 'none';
    $text_display = ($values['type'] == 'text') ? '' : 'none';
  }
  else {
    $id = sizeof($content);
    $values = array();
    $image_display = '';
    $text_display = 'none';
  }

  if (isset($form_state['input']['type'])) {
    if ($form_state['input']['type'] == 'text') {
      $image_display = 'none';
      $text_display = '';
    }
    else {
      $image_display = '';
      $text_display = 'none';
    }
  }

  // jQuery function to switch type.
  drupal_add_js("(function ($) {
      switchAccordionType = function (image_id) {
        $('#horizontal_accordion_content_image').toggle();
        $('#horizontal_accordion_content_text').toggle();
      };
    })(jQuery);", 'inline');

  $form['id'] = array('#type' => 'hidden', '#value' => $id);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('title'),
    '#default_value' => isset($values['title']) ? $values['title'] : '');

  $form['type'] = array(
    '#type' => 'radios',
    '#title' => 'Please select the type of entry',
    '#options' => array('image' => 'Image', 'text' => 'Text'),
    '#default_value' => ($image_display == '') ? 'image' : 'text',
    '#attributes' => array('onchange' => 'switchAccordionType()'));

  $form['content']['image'] = array(
    '#type' => 'media',
    '#title' => '',
    '#description' => '',
    '#required' => FALSE,
    '#tree' => TRUE,
    '#default_value' => (isset($values['content']['fid']) ? array('fid' => $values['content']['fid']) : ''),
    '#media_options' => array(
      'global' => array(
        'types' => array('image'),
        'schemes' => array(
          'http',
          'ftp',
          'flickr',
        ),
      ),
    ),
    '#prefix' => '<div id="horizontal_accordion_content_image" style="display: ' . $image_display .
    '">',
  );

  $form['content']['link'] = array(
    '#type' => 'textfield',
    '#title' => t('Create a link'),
    '#default_value' => isset($values['content']['link']) ? $values['content']['link'] : '',
  );

  $form['content']['caption'] = array(
    '#type' => 'textfield',
    '#title' => t('Caption'),
    '#default_value' => isset($values['content']['caption']) ? $values['content']['caption'] : '',
    '#suffix' => '</div>',
  );

  $form['content']['text'] = array(
    '#type' => 'text_format',
    '#default_value' => (isset($values['content']['text']) ? $values['content']['text'] : ''),
    '#prefix' => '<div id="horizontal_accordion_content_text" style="display: ' . $text_display .
    '">',
    '#suffix' => '</div>');

  return system_settings_form($form);
}

/**
 * Callback horizontal_accordion_edit_specific_element () validate function.
 */
function horizontal_accordion_edit_specific_element_validate($form, &$form_state) {
  $values = $form_state['values'];
  $content = horizontal_accordion_get_content();

  $id = $values['id'];
  $type = $values['type'];
  if ($type == 'image') {
    $main_content = array(
      'fid' => $values['image']['fid'],
      'link' => $values['link'],
      'caption' => $values['caption'],
    );
    if ($main_content['fid'] == 0) {
      form_set_error('image', 'No image uploaded');
      return;
    }
    elseif ($main_content['link'] != '' && !valid_url($main_content['link'])) {
      form_set_error('link', 'The url entered is not valid');
      return;
    }
  }
  else {
    $main_content = array('text' => $values['text']['value']);
  }

  $content[$id] = array(
    'title' => $values['title'],
    'type' => $type,
    'content' => $main_content);

  variable_set('horizontal_accordion_elements', $content);

  // Update the style properties.
  horizontal_accordion_image_style_update();

  drupal_goto('admin/structure/horizontal_accordion');
}

/**
 * Verify if the user actually wants to delete an element.
 */
function horizontal_accordion_delete_specific_element($form, &$form_state) {
  $id = arg(4);
  $content = horizontal_accordion_get_content();
  if (isset($content[$id])) {
    $form['id'] = array('#type' => 'hidden', '#value' => $id);
    return confirm_form($form, t('Are you sure you want to delete this element ?'),
      'admin/structure/horizontal_accordion');
  }
}

/**
 * Callback horizontal_accordion_delete_specific_element submit function.
 */
function horizontal_accordion_delete_specific_element_submit($form, &$form_state) {
  $content = horizontal_accordion_get_content();
  $id = $form_state['values']['id'];

  unset($content[$id]);
  variable_set('horizontal_accordion_elements', $content);

  // Update the style properties.
  horizontal_accordion_image_style_update();

  drupal_goto('admin/structure/horizontal_accordion');
}
